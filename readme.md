The ITSAPE (Windows) Client
===========================

## INSTALLATION
```
msiexec /i C:\path\to\ITSAPEClientServiceSetup.msi /quiet /qn
```

## LIMITATIONS
The Client only works on Windows with **German** or **English** language settings enabled **globally**!
