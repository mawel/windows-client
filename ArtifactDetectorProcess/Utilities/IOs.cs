﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ItsApe.ArtifactDetectorProcess.Utilities
{
    public interface IOs
    {
        bool CloseHandle(IntPtr handle);
        
        bool EnumWindows(NativeMethods.EnumWindowsProc enumFunc, IntPtr lParam);

        IntPtr FindWindow(string lpszClass, string lpszWindow);
        
        IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        bool GetWindowInfo(IntPtr hwnd, ref NativeMethods.WindowVisualInformation pwi);

        bool GetWindowRect(IntPtr hWnd, ref ArtifactDetector.Utilities.NativeStructures.RectangularOutline rect);

        int GetWindowText(IntPtr hWnd, StringBuilder lpString, [MarshalAs(UnmanagedType.U4)] int nMaxCount);

        int GetWindowTextLength(IntPtr hWnd);

        uint GetWindowThreadProcessId(IntPtr hWnd, [Out] out uint dwProcessId);

        bool IsIconic(IntPtr hWnd);

        bool IsWindowVisible(IntPtr hWnd);

        IntPtr OpenProcess(uint dwDesiredAccess, bool bInheritHandle, uint dwProcessId);

        bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, UIntPtr nSize, ref uint vNumberOfBytesRead);

        IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, uint flAllocationType, uint flProtect);

        bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, uint dwFreeType);

        bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, UIntPtr nSize, ref uint vNumberOfBytesRead);
        
        public int GetSystemMetrics(int smIndex);
        
        public int SetProcessDpiAwareness(int value);
    }
}
