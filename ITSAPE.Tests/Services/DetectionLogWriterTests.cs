using System;
using System.IO;
using ItsApe.ArtifactDetector.Models;
using ItsApe.ArtifactDetector.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace ITSAPE.Tests.Services
{
    public class DetectionLogWriterTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public DetectionLogWriterTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void Ctor_SetsUpFilePath()
        {
            var dir = GetTempWorkdir();
            var artifactName = "TestArtifact";
            var artifactDir = Path.Combine(dir, artifactName);
            var logger = new Mock<ILogger<DetectionLogWriter>>();

            var writer = new DetectionLogWriter(dir, artifactName, logger.Object);


            Assert.True(Directory.Exists(artifactDir));
            var files = Directory.GetFiles(artifactDir, "*.csv");
            Assert.Single(files);
            Assert.Contains(writer.currentLogFile, files);
            
            Directory.Delete(dir, true);
        }
        

        [Fact]
        public void LogDetectionResult_LogsResult()
        {
            var dir = GetTempWorkdir();
            var artifactName = "TestArtifact";
            var artifactDir = Path.Combine(dir, artifactName);
            var logger = new Mock<ILogger<DetectionLogWriter>>();
            var writer = new DetectionLogWriter(dir, artifactName, logger.Object);
            var time = DateTime.Now;
            var response = new DetectorResponse()
            {
                ArtifactPresent = DetectorResponse.ArtifactPresence.Certain,
                timestamps = new [] // idk what this is
                {
                    (time - TimeSpan.FromSeconds(1)).ToFileTimeUtc(),
                    (time - TimeSpan.FromSeconds(2)).ToFileTimeUtc(),
                    (time - TimeSpan.FromSeconds(3)).ToFileTimeUtc(),
                    (time - TimeSpan.FromSeconds(4)).ToFileTimeUtc(),
                }
            };
            
            writer.LogDetectionResult(time, response);
            

            Assert.True(Directory.Exists(artifactDir));
            var files = Directory.GetFiles(artifactDir, "*.csv");
            Assert.Single(files);
            Assert.Contains(writer.currentLogFile, files);
            Assert.StartsWith(time.ToString("yyMMddHHmmssfff"), File.ReadAllText(writer.currentLogFile));
            
            Directory.Delete(dir, true);
        }
        
        [Fact]
        public void CompileResponses_WritesFile()
        {
            var dir = GetTempWorkdir();
            var artifactName = "TestArtifact";
            var artifactDir = Path.Combine(dir, artifactName);
            var logger = new Mock<ILogger<DetectionLogWriter>>();
            var writer = new DetectionLogWriter(dir, artifactName, logger.Object);
            var time = DateTime.Now;
            
            var file = writer.CompileResponses(3);
            

            Assert.True(Directory.Exists(artifactDir));
            var files = Directory.GetFiles(artifactDir, "*.csv");
            Assert.Equal(2, files.Length);
            Assert.Contains(writer.currentLogFile, files);
            Assert.True(File.Exists(file));
            
            Directory.Delete(dir, true);
        }
        
        
        private string GetTempWorkdir()
        {
            var rnd = new Random();
            string path;

            do
            {
                path = "/tmp/test-" + rnd.Next();
            } while (Directory.Exists(path));

            Directory.CreateDirectory(path);

            return path;
        }
    }
}