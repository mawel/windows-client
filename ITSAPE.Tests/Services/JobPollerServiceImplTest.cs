﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using ITSAPE.Client.Services;
using ITSAPE_Client.Jobs;
using ITSAPE_Client.Models;
using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using ISetup = ITSAPE_Client.Utilities.ISetup;
using Timer = System.Timers.Timer;

namespace ITSAPE.Tests.Services
{
    public class JobPollerServiceImplTest
    {

        [Fact]
        public void OnStart_AbortsOnMissingConfig()
        {
            var logger = new Mock<ILogger<JobPollerServiceImpl>>();
            var poller = new Mock<IJobPoller>();
            var setup = new Mock<ISetup>();
            setup.Setup(e => e.GetConfiguration())
                .Throws<FileNotFoundException>()
                .Verifiable();

            var service = new JobPollerServiceImpl(logger.Object, setup.Object, poller.Object);


            service.StartAsync(CancellationToken.None).Wait();


            setup.Verify(e => e.GetConfiguration(), Times.Once);
            logger.Verify(e =>
                e.Log(LogLevel.Error,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => v.ToString().Contains("Cannot load config: Client will not be executed.")),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Once);
        }

        [Fact]
        public void OnStart_StartsJobPollTimer()
        {
            var logger = new Mock<ILogger<JobPollerServiceImpl>>();
            var poller = new Mock<IJobPoller>();
            var setup = new Mock<ISetup>();
            setup.Setup(e => e.GetConfiguration())
                .Returns(new Configuration()
                {
                    CertThumbprint = "",
                    ConnectivityInterval = 2000,
                    DnsSetting = new Configuration.Dns(),
                    GatewaySetting = new Configuration.Gateway(),
                    ProxySetting = new Configuration.UProxy
                    {
                        Host = "localhost",
                        Interval = 2000,
                        Port = 1234
                    }
                })
                .Verifiable();
            setup.Setup(e => e.GetInstallPath())
                .Returns(Environment.CurrentDirectory);

            var service = new JobPollerServiceImpl(logger.Object, setup.Object, poller.Object);


            service.StartAsync(CancellationToken.None).Wait();

            Assert.True(service.jobPollTimer.Enabled);
            setup.Verify(e => e.GetConfiguration(), Times.Once);
        }

        [Fact]
        public void Stop_DisposesJobPollTimer()
        {
            var logger = new Mock<ILogger<JobPollerServiceImpl>>();
            var setup = new Mock<ISetup>();
            var poller = new Mock<IJobPoller>();
            var timer = new Timer();
            var service = new JobPollerServiceImpl(logger.Object, setup.Object, poller.Object)
            {
                jobPollTimer = timer
            };

            timer.Start();
            Assert.True(timer.Enabled);

            service.StopAsync(CancellationToken.None).Wait();


            Assert.False(timer.Enabled);
        }
        

        [Fact]
        public void Stop_InformsUser()
        {
            var logger = new Mock<ILogger<JobPollerServiceImpl>>();
            var setup = new Mock<ISetup>();
            var poller = new Mock<IJobPoller>();
            var service = new JobPollerServiceImpl(logger.Object, setup.Object, poller.Object);


            service.StopAsync(CancellationToken.None).Wait();


            logger.Verify(e =>
                e.Log(LogLevel.Information,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((v, t) => v.ToString().Contains("ITSAPE-Client service stopped.")),
                    It.IsAny<Exception>(),
                    It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Once);
        }
    }
}
