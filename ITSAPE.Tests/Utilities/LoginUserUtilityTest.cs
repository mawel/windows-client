﻿using System;
using ITSAPE.Client.Utilities;
using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace ITSAPE.Tests.Utilities
{
    public class LoginUserUtilityTest
    {

        private const string QueryCommandOutput =
@" USERNAME              SESSIONNAME        ID  STATE   IDLE TIME  LOGON TIME
 win10 vm              console             1  Active      none   29/07/2021 16:29";


        [Fact]
        public void GetLoggedInUser_ReturnsUserName()
        {
            var logger = Mock.Of<ILogger<LoginUserUtility>>();
            var systemMock = new Mock<ISystemHelper>();
            systemMock.Setup(e => e.GetLoggedInUserInformation())
                .Returns(QueryCommandOutput);
            var util = new LoginUserUtility(logger, systemMock.Object);


            var user = util.GetLoggedInUser();


            systemMock.Verify(e => e.GetLoggedInUserInformation());
            Assert.Equal("win10 vm", user);
        }

        [Fact]
        public void GetLoggedInUser_ReturnsSessionId()
        {
            var logger = Mock.Of<ILogger<LoginUserUtility>>();
            var systemMock = new Mock<ISystemHelper>();
            systemMock.Setup(e => e.GetLoggedInUserInformation())
                .Returns(QueryCommandOutput);
            var util = new LoginUserUtility(logger, systemMock.Object);


            var sid = util.GetUserSessionID();


            systemMock.Verify(e => e.GetLoggedInUserInformation());
            Assert.Equal(1u, sid);
        }
    }
}
