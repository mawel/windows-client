﻿using ITSAPE_Client.Models;
using ITSAPE_Client.Utilities;
using Moq;
using System;
using Microsoft.Extensions.Logging;
using Xunit;
using ISetup = ITSAPE_Client.Utilities.ISetup;

namespace ITSAPE.Tests.Utilities
{
    public class NetworkManagerTest
    {
        [Fact]
        public void AddConfiguration_AddsNewConfiguration()
        {
            var logger = Mock.Of<ILogger<NetworkManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(e => e.GetInstallPath())
                .Returns(Environment.CurrentDirectory);
            var newConfig = new NetworkConfiguration("8.8.8.8", "127.0.0.1", 5000, 3600);
            var m = new NetworkManager(logger, setup.Object);


            var rs = m.AddConfiguration("newConfig", newConfig, isCurrentConfig: false);


            var getVal = m.networkConfigurations.TryGetValue("newConfig", out var savedConfig);

            Assert.True(rs);
            Assert.True(getVal);
            Assert.Equal(newConfig, savedConfig);
        }

        [Fact]
        public void AddConfiguration_DoesNotAddWithNameCurrent()
        {
            var logger = Mock.Of<ILogger<NetworkManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(e => e.GetInstallPath())
                .Returns(Environment.CurrentDirectory);
            var newConfig = new NetworkConfiguration("8.8.8.8", "127.0.0.1", 5000, 3600);
            var m = new NetworkManager(logger, setup.Object);


            var rs = m.AddConfiguration("current", newConfig, isCurrentConfig: false);


            var getVal = m.networkConfigurations.TryGetValue("current", out var savedConfig);

            Assert.False(rs);
            Assert.True(getVal);
            Assert.NotEqual(newConfig, savedConfig);
        }

        [Fact]
        public void Change_ChangesCurrentConfiguration()
        {
            var logger = Mock.Of<ILogger<NetworkManager>>();
            var setup = new Mock<ISetup>();
            setup.Setup(e => e.GetInstallPath())
                .Returns(Environment.CurrentDirectory);
            var otherConfig = new NetworkConfiguration("8.8.8.8", "127.0.0.1", 5000, 3600);
            var m = new NetworkManager(logger, setup.Object);

            var addSuccess = m.AddConfiguration("other", otherConfig);

            
            m.Change("other");


            Assert.True(addSuccess);
            Assert.Equal(otherConfig, m.networkConfigurations["current"]);
        }


    }
}
