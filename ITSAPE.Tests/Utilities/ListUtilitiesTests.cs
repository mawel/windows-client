using System.Collections.Generic;
using ITSAPE.Client.Utilities;
using Xunit;

namespace ITSAPE.Tests.Utilities
{
    public class ListUtilitiesTests
    {
        [Fact]
        public void GetMajorityItem_GetsMajorityItem()
        {
            var list = new List<KeyValuePair<long, int>>()
            {
                new(1u, 2),
                new(1u, 2),
                new(1u, 2),
                new(1u, 3),
                new(1u, 3)
            };


            var item = list.GetMajorityItem();
            
            
            Assert.Equal(2, item);
        }
    }
}