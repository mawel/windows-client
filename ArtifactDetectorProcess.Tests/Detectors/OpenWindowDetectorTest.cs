﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using ItsApe.ArtifactDetector.Models;
using ItsApe.ArtifactDetectorProcess.Detectors;
using ItsApe.ArtifactDetectorProcess.Utilities;
using Moq;
using Xunit;

namespace ArtifactDetectorProcess.Tests.Detectors
{
    public class OpenWindowDetectorTest
    {
        delegate bool GetWindowInfoCallback(IntPtr hWnd, ref NativeMethods.WindowVisualInformation info);
        [Fact]
        public void AnalyzeVisibleWindowDelegate_IgnoreInvisible()
        {
            var wndPtr = new IntPtr(1);
            var os = new Mock<IOs>();
            os.Setup(o => o.IsWindowVisible(wndPtr)).Returns(false);
            var info = new ArtifactRuntimeInformation();
            var infoHandle = GCHandle.Alloc(info);
            var infoPtr = GCHandle.ToIntPtr(infoHandle);
            var det = new OpenWindowDetector(os.Object);

            var continueEnumeration = det.AnalyzeVisibleWindowDelegate(wndPtr, infoPtr);

            Assert.True(continueEnumeration);
            Assert.Empty(info.VisibleWindowOutlines);
            Assert.Equal(0, info.MaxWindowVisibilityPercentage);

            infoHandle.Free();
        }

        [Fact]
        public void AnalyzeVisibleWindowDelegate_IgnoreIconic()
        {
            var wndPtr = new IntPtr(1);
            var os = new Mock<IOs>();
            os.Setup(o => o.IsWindowVisible(wndPtr)).Returns(true);
            os.Setup(o => o.IsIconic(wndPtr)).Returns(true);
            var info = new ArtifactRuntimeInformation();
            var infoHandle = GCHandle.Alloc(info);
            var infoPtr = GCHandle.ToIntPtr(infoHandle);
            var det = new OpenWindowDetector(os.Object);

            var continueEnumeration = det.AnalyzeVisibleWindowDelegate(wndPtr, infoPtr);

            Assert.True(continueEnumeration);
            Assert.Empty(info.VisibleWindowOutlines);
            Assert.Equal(0, info.MaxWindowVisibilityPercentage);

            infoHandle.Free();
        }

        [Fact]
        public void AnalyzeVisibleWindowDelegate_IgnoreNoTitle()
        {
            var wndPtr = new IntPtr(1);
            var os = new Mock<IOs>();
            os.Setup(o => o.IsWindowVisible(wndPtr)).Returns(true);
            os.Setup(o => o.IsIconic(wndPtr)).Returns(false);
            os.Setup(o => o.GetWindowTextLength(wndPtr)).Returns(0);
            var info = new ArtifactRuntimeInformation();
            var infoHandle = GCHandle.Alloc(info);
            var infoPtr = GCHandle.ToIntPtr(infoHandle);
            var det = new OpenWindowDetector(os.Object);

            var continueEnumeration = det.AnalyzeVisibleWindowDelegate(wndPtr, infoPtr);

            Assert.True(continueEnumeration);
            Assert.Empty(info.VisibleWindowOutlines);
            Assert.Equal(0, info.MaxWindowVisibilityPercentage);

            infoHandle.Free();
        }

        [Fact]
        public void AnalyzeVisibleWindowDelegate_AddsWindow()
        {
            var wndPtr = new IntPtr(1);
            var os = new Mock<IOs>();
            os.Setup(o => o.IsWindowVisible(wndPtr)).Returns(true);
            os.Setup(o => o.IsIconic(wndPtr)).Returns(false);
            os.Setup(o => o.GetWindowTextLength(wndPtr)).Returns(1);
            os.Setup(o => o.GetWindowInfo(wndPtr, ref It.Ref<NativeMethods.WindowVisualInformation>.IsAny))
                .Returns(new GetWindowInfoCallback((IntPtr hWnd, ref NativeMethods.WindowVisualInformation si) =>
                {
                    si.dwStyle = NativeMethods.WindowStyles.WS_MAXIMIZE;
                    si.dwExStyle = NativeMethods.WindowStyles.WS_EX_APPWINDOW;
                    return true;
                }));
            os.Setup(o => o.GetWindowText(wndPtr, It.IsAny<StringBuilder>(), It.IsAny<int>())).Returns(1);
            var info = new ArtifactRuntimeInformation();
            var infoHandle = GCHandle.Alloc(info);
            var infoPtr = GCHandle.ToIntPtr(infoHandle);
            var det = new OpenWindowDetector(os.Object);

            var continueEnumeration = det.AnalyzeVisibleWindowDelegate(wndPtr, infoPtr);

            Assert.True(continueEnumeration);
            Assert.NotEmpty(info.VisibleWindowOutlines);

            infoHandle.Free();
        }
    }
}
