using System;
using System.Text;
using ItsApe.ArtifactDetectorProcess.Detectors;
using ItsApe.ArtifactDetectorProcess.Utilities;
using Moq;
using Xunit;

namespace ArtifactDetectorProcess.Tests.Detectors
{
    public class DesktopIconDetectorTests
    {
        [Fact]
        public void IconTitleFromBuffer_ReadsString()
        {
            var origStr = "IconTitle";
            var origEncoded = Encoding.Unicode.GetBytes(origStr + "\0");
            var os = new Mock<IOs>();
            var detector = new DesktopIconDetector(os.Object);
            Array.Copy(origEncoded, detector._buffer, origEncoded.Length);

            
            var str = detector.IconTitleFromBuffer(origEncoded.Length);
            
            
            Assert.Equal(origStr, str);
        }
    }
}