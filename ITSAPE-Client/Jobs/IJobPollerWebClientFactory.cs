﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITSAPE.Client.Jobs
{
    public interface IJobPollerWebClientFactory
    {
        IJobPollerWebClient CreateClient();
    }
}
