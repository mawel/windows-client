﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITSAPE.Client.Jobs
{
    public interface IJobPollerWebClient : IDisposable
    {
        string DownloadString(string uri);
        string UploadString(string uri, string data);
        void DownloadFile(string uri, string targetFile);
        byte[] UploadFile(string uri, string sourceFile);
    }
}
