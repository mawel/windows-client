﻿using ITSAPE_Client.Models;
using ITSAPE_Client.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Timers;
using System.Web;
using ITSAPE.Client.Jobs;
using ITSAPE.Client.Utilities;
using Microsoft.Extensions.Logging;

namespace ITSAPE_Client.Jobs
{
    public class JobPoller : IJobPoller
    {

        /// <summary>
        /// Logger to write log messages to.
        /// </summary>
        private readonly ILogger eventLog;

        /// <summary>
        /// Controller to actually do something with the jobs.
        /// </summary>
        private readonly IJobManager jobManager;


        /// <summary>
        /// Flag for error during last job request.
        /// </summary>
        private bool hadRequestJobsError = false;

        private readonly IJobPollerWebClientFactory _clientFactory;

        /// <summary>
        /// Constructor for this job poller setting all necessary properties.
        /// </summary>
        /// <param name="jobServerAddress">Address of the server to poll for jobs.</param>
        /// <param name="certThumbprint">Thumbprint of job server certificate.</param>
        /// <param name="eventLog">Logger to write log messages to.</param>
        public JobPoller(ILogger<JobPoller> eventLog, ILoginUserUtility loginUserUtility, IJobManager jobManager, IJobPollerWebClientFactory clientFactory)
        {
            this.eventLog = eventLog;

            LoginUserController = loginUserUtility;
            this.jobManager = jobManager;
            _clientFactory = clientFactory;

            eventLog.LogInformation("Start job polling.");
        }

        /// <summary>
        /// Controller to get information about the logged in user.
        /// </summary>
        private ILoginUserUtility LoginUserController { get; set; }

        /// <summary>
        /// Timer callback to query jobs from the job server, download and execute them if necessary.
        /// </summary>
        /// <param name="sender">Timer object.</param>
        /// <param name="e">Arguments for this event.</param>
        public void GetAndExecuteJobs(object sender, ElapsedEventArgs e)
        {
            try
            {
                jobManager.ExecuteJobs(RequestJobs());
            }
            catch (Exception error)
            {
                eventLog.LogError(error, "JobPoller failed!");
            }
        }

        /// <summary>
        /// Deserialize a JSON request string to a Job object.
        /// </summary>
        /// <param name="request">JSON request.</param>
        /// <returns>The new Job object instance.</returns>
        public Job ConvertToJob(string request)
        {
            // Use the Newtonsoft JSON library to deserialize the request.
            return JsonConvert.DeserializeObject<Job>(request);
        }

        /// <summary>
        /// Retrieve a list of all jobs given by their job URIs.
        /// </summary>
        /// <param name="jobURIs">URIs of jobs to retrieve.</param>
        /// <returns>List of jobs.</returns>
        public IList<Job> GetAllJobs(List<string> jobURIs)
        {
            var jobList = new List<Job>();
            using var webClient = _clientFactory.CreateClient();

            // Get all jobs by job URI.
            foreach (var uri in jobURIs)
            {
                try
                {
                    // Get raw JSON response from this URI.
                    var response = webClient.DownloadString(uri);
                    // Convert JSON to Job object.
                    Job job = ConvertToJob(response);
                    jobList.Add(job);
                }
                catch (Exception e)
                {
                    eventLog.LogError(e, "Cannot download job from proxy. uri: " + uri);
                }
            }

            return jobList;
        }

        /// <summary>
        /// Get all jobs for currently logged in user.
        /// </summary>
        /// <returns>List of jobs, empty on error.</returns>
        public IList<Job> RequestJobs()
        {
            string username = LoginUserController.GetLoggedInUser();

            // If no user is currently logged in, return an empty list.
            if (username == "")
            {
                return new List<Job>();
            }

            string jobRequestUri = "jobs/" + HttpUtility.UrlEncode(username);

            using var webClient = _clientFactory.CreateClient();

            // Get list of jobs for this user
            try
            {
                var request = webClient.DownloadString(jobRequestUri);
                List<string> jobURIs = JsonConvert.DeserializeObject<List<string>>(request);

                if (jobURIs != null)
                {
                    var jobList = GetAllJobs(jobURIs);
                    hadRequestJobsError = false;
                    return jobList;
                }
                else
                {
                    hadRequestJobsError = false;
                    return new List<Job>();
                }
            }
            catch (WebException e)
            {
                if (!hadRequestJobsError)
                {
                    eventLog.LogError(e, $"Cannot download job list from proxy (check if the proxy is running and if TLS is configured well). uri: {jobRequestUri}\n{ e.Message}\nResponse: {e.Response}\n{e.Status}");
                    hadRequestJobsError = true;
                }
                return new List<Job>();
            }
            catch (ArgumentNullException e)
            {
                if (!hadRequestJobsError)
                {
                    eventLog.LogError(e, "Cannot download job list from proxy (uri is null).");
                    hadRequestJobsError = true;
                }
                return new List<Job>();
            }
        }
    }
}