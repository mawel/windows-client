﻿using System;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using ITSAPE.Client.Jobs;
using Microsoft.Extensions.Logging;

namespace ITSAPE_Client.Jobs
{
    internal class JobPollerWebClient : WebClient, IJobPollerWebClient
    {
        /// <summary>
        /// Server certificate thumbprint.
        /// </summary>
        private readonly string certificateThumbprint;

        /// <summary>
        /// Event log for debugging.
        /// </summary>
        private ILogger eventLog;

        /// <summary>
        /// Web client used by the job poller.
        /// </summary>
        /// <param name="eventLog">Event log for debugging.</param>
        /// <param name="certificateThumbprint">Server certificate thumbprint.</param>
        public JobPollerWebClient(ILogger<JobPollerWebClient> eventLog, string certificateThumbprint)
        {
            this.eventLog = eventLog;
            this.certificateThumbprint = certificateThumbprint;
        }

        /// <summary>
        /// Perform new HTTP request to the given URI address.
        /// </summary>
        /// <param name="address">Address to request.</param>
        /// <returns>Raw request.</returns>
        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);

            X509Store store = new X509Store(
                StoreName.My,
#if DEBUG
                StoreLocation.CurrentUser
#else
            StoreLocation.LocalMachine
#endif
                );
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2 cert;
            try
            {
                X509Certificate2Collection certificates = store.Certificates.Find(X509FindType.FindByThumbprint, certificateThumbprint, false);
                if (certificates.Capacity == 0)
                {
                    string errorMessage = $"There is no certificate with that thumbprint: {certificateThumbprint}";
                    eventLog.LogError(errorMessage + "\n\n" + certificates.ToString());
                    throw new WebException(errorMessage);
                }
                cert = certificates[0];
            }
            finally
            {
                store.Close();
            }

            request.ClientCertificates.Add(cert);

            return request;
        }
    }
}