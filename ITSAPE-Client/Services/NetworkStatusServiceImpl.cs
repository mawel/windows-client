﻿using ITSAPE_Client.Models;
using ITSAPE_Client.Networks;
using ITSAPE_Client.Utilities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Timers;
using System.Threading.Tasks;
using ITSAPE.Client.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Timer = System.Timers.Timer;

[assembly: InternalsVisibleTo("ITSAPE.Tests")]
namespace ITSAPE.Client.Services
{
    public class NetworkStatusServiceImpl : IHostedService, IDisposable
    {
        /// <summary>
        /// Instance of connectivity checker to use for ConnectionCheckTimer.
        /// </summary>
        private IConnectivityChecker connectivityChecker;

        /// <summary>
        /// Timer to periodically check the connectivity.
        /// </summary>
        internal Timer connectivityCheckTimer;

        /// <summary>
        /// Network manager mainly used for DNS settings.
        /// </summary>
        private readonly INetworkManager networkManager;

        private readonly ILogger _logger;
        private readonly ISetup _setup;
        private readonly IOs _os;
        private IServiceProvider _services;

        public NetworkStatusServiceImpl(ILogger<NetworkStatusServiceImpl> logger, ISetup setup, IOs os, IServiceProvider services, INetworkManager networkManager, IConnectivityChecker connectivityChecker)
        {
            _logger = logger;
            _setup = setup;
            _os = os;
            _services = services;
            this.networkManager = networkManager;
            this.connectivityChecker = connectivityChecker;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {

            // Loading configuration or stop client if unable to load.
            Configuration configuration;
            try
            {
                configuration = _setup.GetConfiguration();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Cannot load config: Client will not be executed.");
                await StopAsync(CancellationToken.None);
                return;
            }

            // If we are here the config has been loaded successfully.
            _logger.LogInformation("Config: " + JsonConvert.SerializeObject(configuration));
            
            //TODO: Check if dns-file is present

            // Store DNS settings to default config.
            networkManager.AddConfiguration("default", new NetworkConfiguration("", configuration.GatewaySetting.Standard), true);
            // Add itsape DNS config.
            networkManager.AddConfiguration("itsape", new NetworkConfiguration(configuration.DnsSetting.ItsApe, configuration.GatewaySetting.ItsApe));

            // Start timer for framework state checking.
            connectivityCheckTimer = new Timer
            {
                Interval = configuration.ConnectivityInterval,
                AutoReset = true,
                Enabled = true
            };
            connectivityCheckTimer.Elapsed += new ElapsedEventHandler(connectivityChecker.CheckStatus);
            connectivityCheckTimer.Start();

            _logger.LogInformation("ITSAPE-Client v1.1.5 network status service started. ");
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop thread for framework state checking.
            connectivityCheckTimer?.Stop();
            connectivityCheckTimer?.Dispose();
            connectivityCheckTimer = null;

            // Reset the network configuration.
            if (networkManager != null)
            {
                networkManager.Change("default");
                networkManager.CheckInterfaces();

                // Now delete dnsSettings.json to mark service as successfully stopped.
                networkManager.DeleteDNSSettingsFile();
            }

            _logger.LogInformation("ITSAPE-Client service stopped.");
        }

        public void Dispose()
        {
            connectivityCheckTimer?.Dispose();
        }
    }
}
