using System;
using System.Collections.Generic;
using System.ServiceProcess;

namespace ITSAPE.Client.Services
{
    public interface ILifetimeEventDispatcher
    {
        /// <summary>
        /// Continueing when the service was paused by the OS.
        /// </summary>
        void DispatchContinue();

        /// <summary>
        /// Pause signal from the OS.
        /// </summary>
        void DispatchPause();

        /// <summary>
        /// Power event from the OS.
        /// </summary>
        /// <param name="powerStatus">Information about the new status.</param>
        void DispatchPowerEvent(PowerBroadcastStatus powerStatus);

        /// <summary>
        /// Method is called by system whenever a session is changed.
        /// </summary>
        /// <param name="changeDescription">Struct with further info about the change.</param>
        void DispatchSessionChange(SessionChangeDescription changeDescription);

        /// <summary>
        /// Handle shutdown of service by system.
        /// </summary>
        void DispatchShutdown();

        void AddHandler(ILifetimeEventHandler handler);
        void RemoveHandler(ILifetimeEventHandler handler);
    }

    public class LifetimeEventDispatcher : IDisposable, ILifetimeEventDispatcher
    {
        public List<ILifetimeEventHandler> Handlers { get; } = new();

        /// <summary>
        /// Continueing when the service was paused by the OS.
        /// </summary>
        public void DispatchContinue()
        {
            foreach (var handler in Handlers)
            {
                handler.OnContinue();
            }
        }

        /// <summary>
        /// Pause signal from the OS.
        /// </summary>
        public void DispatchPause()
        {
            foreach (var handler in Handlers)
            {
                handler.OnPause();
            }
        }

        /// <summary>
        /// Power event from the OS.
        /// </summary>
        /// <param name="powerStatus">Information about the new status.</param>
        public void DispatchPowerEvent(PowerBroadcastStatus powerStatus)
        {
            foreach (var handler in Handlers)
            {
                handler.OnPowerEvent(powerStatus);
            }
        }

        /// <summary>
        /// Method is called by system whenever a session is changed.
        /// </summary>
        /// <param name="changeDescription">Struct with further info about the change.</param>
        public void DispatchSessionChange(SessionChangeDescription changeDescription)
        {
            foreach (var handler in Handlers)
            {
                handler.OnSessionChange(changeDescription);
            }
        }

        /// <summary>
        /// Handle shutdown of service by system.
        /// </summary>
        public void DispatchShutdown()
        {
            foreach (var handler in Handlers)
            {
                handler.OnShutdown();
            }
        }

        public void AddHandler(ILifetimeEventHandler handler)
        {
            Handlers.Add(handler);
        }

        public void RemoveHandler(ILifetimeEventHandler handler)
        {
            Handlers.Remove(handler);
        }

        public void Dispose()
        {
            foreach (var handler in Handlers)
            {
                if(handler is IDisposable disp)
                    disp.Dispose();
            }
            
            Handlers.Clear();
        }
    }
}