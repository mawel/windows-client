﻿using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace ITSAPE_Client.Service
{
    public interface IDetectorService : IHostedService
    {
        
        bool StartWatch(string jsonEncodedParameters);

        

        
        string StopWatch(string jsonEncodedParameters);

        
    }
}