﻿using System.Timers;

namespace ITSAPE_Client.Networks
{
    public interface IConnectivityChecker
    {
        /// <summary>
        /// Flushes hostName from the DNS cache.
        /// </summary>
        /// <param name="hostName">The hostName to flush from the cache</param>
        void FlushCache(string hostName);

        /// <summary>
        /// Check the status of the connectivity (to the internet).
        /// To be called periodically by a Timer object.
        /// </summary>
        /// <param name="state">State of the timer.</param>
        /// <param name="e">Arguments for the elapsed event.</param>
        void CheckStatus(object state, ElapsedEventArgs e);
    }
}