﻿using ITSAPE_Client.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Text;

[assembly: InternalsVisibleTo("ITSAPE.Tests")]
namespace ITSAPE_Client.Utilities
{
    public class NetworkManager : INetworkManager
    {
        /// <summary>
        /// Name of the file containing the JSON-encoded default DNS settings.
        /// </summary>
        private const string defaultDnsSettingsFile = "dnsSettings.json";

        /// <summary>
        /// Registry key where network interface configurations are stored.
        /// </summary>
        private const string interfaceConfigurationRegistryKey = @"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces\";

        /// <summary>
        /// Path to the DNS config file.
        /// </summary>
        private readonly string dnsFilePath;

        /// <summary>
        /// Event log for debugging.
        /// </summary>
        private ILogger eventLog;

        /// <summary>
        /// List of tuples:
        ///   Interface ID => DNS settings
        /// </summary>
        private IDictionary<string, string> interfaceDnsSettingsMap;

        /// <summary>
        /// Network configurations accessable by string key.
        /// </summary>
        internal Dictionary<string, NetworkConfiguration> networkConfigurations;

        /// <summary>
        /// List of network interfaces.
        /// </summary>
        private List<string> networkInterfaces;

        /// <summary>
        /// Constructs a new NetworkManager.
        /// </summary>
        /// <param name="eventlLog">EventLog to write messages to.</param>
        public NetworkManager(ILogger<NetworkManager> eventLog, ISetup setup)
        {
            this.eventLog = eventLog;
            dnsFilePath = Path.Combine(setup.GetInstallPath(), defaultDnsSettingsFile);

            networkConfigurations = new Dictionary<string, NetworkConfiguration>
            {
                ["current"] = new NetworkConfiguration("", "")
            };

            networkInterfaces = new List<string>();
            interfaceDnsSettingsMap = GetDefaultDNSSettings();
        }

        private delegate void InterfaceIteratorCallback(ManagementObject managementObject);

        /// <summary>
        /// Interval for checking the connectivity.
        /// </summary>
        public int ConnectivityInterval { get; set; }


        /// <summary>
        /// Add the NetworkConfiguration with name to the NetworkManager. If the name already exists or equals 'current' the NetworkConfiguration will not be added.
        /// </summary>
        /// <param name="name">The name of the NetworkConfiguration.</param>
        /// <param name="nc">The NetworkConfiguration to store.</param>
        /// <param name="isCurrentConfig">If true the NetworkConnection will be set as the 'current' connection.</param>
        /// <returns>True if the NetworkConnection has been added, false otherwise.</returns>
        public bool AddConfiguration(string name, NetworkConfiguration nc, bool isCurrentConfig = false)
        {
            if (networkConfigurations.ContainsKey(name) || name.Equals("current"))
                return false;

            networkConfigurations.Add(name, nc);

            if (isCurrentConfig)
                networkConfigurations["current"].Set(nc);

            return networkConfigurations.ContainsKey(name);
        }

        /// <summary>
        /// Changes the current configuration.
        /// </summary>
        /// <param name="configName">The configuration to change to.</param>
        public void Change(string configName)
        {
            // Check whether the given config exists.
            if (networkConfigurations.ContainsKey(configName))
            {
                // Change "current" configuration.
                networkConfigurations["current"].Set(networkConfigurations[configName]);

                if (configName == "default")
                {
                    // Check the interfaces and reset the DNS config if needed.
                    CheckInterfaces();
                }
                else
                {
                    // Try to set nameservers for current configuration.
                    try
                    {
                        SetNameservers(networkConfigurations[configName].DnsServers);
                    }
                    catch (Exception e)
                    {
                        eventLog.LogWarning("Cannot set DNS for '" + configName + "' with exception '" + e.Message + "'.");
                    }
                }
            }
            else
            {
                // Given config does not exist: Log error and do nothing.
                eventLog.LogError("Cannot switch to '" + configName + "'. No such config.");
            }
        }

        /// <summary>
        /// Check all network interfaces for their DNS config.
        /// Resets the config according to current network manager.
        /// </summary>
        public void CheckInterfaces()
        {
            try
            {
                IterateInterfaces((managementObject) =>
                {
                    string interfaceId = (string) managementObject["SettingID"];
                    string dnsServers = string.Join(",", GetDnsServersById(interfaceId));

                    if (!IsInterfaceInList(interfaceId)) // is not in list (new interface)
                    {
                        if (IsCurrentNetworkManager("default"))
                        {
                            if (dnsServers.Contains(networkConfigurations["itsape"].DnsServers))
                            {
                                eventLog.LogWarning("I'm in default-config, but dns settings contain itsape-server. I'll set nameservers to auto-config!");
                                SetNameservers("auto", interfaceId);
                                dnsServers = "auto";
                            }
                        }
                        else // currentIs("itsape")
                        {
                            SetNameservers(networkConfigurations["itsape"].DnsServers, interfaceId);
                        }
                        interfaceDnsSettingsMap.Add(interfaceId, dnsServers);
                    }
                    else // is in list (old interface)
                    {
                        if (IsCurrentNetworkManager("default"))
                        {
                            if (dnsServers.Contains(networkConfigurations["itsape"].DnsServers))
                            {
                                var default_dns = GetDnsServer(interfaceId);
                                SetNameservers(default_dns, interfaceId);
                            }
                        }
                        else
                        {
                            if (!dnsServers.Contains(networkConfigurations["itsape"].DnsServers))
                            {
                                eventLog.LogWarning("I'm in itsape-config, but dns settings do not contain itsape-server. I'll set nameservers to itsape-config again!");
                                SetNameservers(networkConfigurations["itsape"].DnsServers, interfaceId);
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                eventLog.LogError("Cannot get default settings for network interfaces.\n\n" + e);
            }
        }

        /// <summary>
        /// Get rid of the DNS settings file.
        /// </summary>
        public void DeleteDNSSettingsFile()
        {
            try
            {
                File.Delete(dnsFilePath);
            }
            catch (Exception e)
            {
                eventLog.LogWarning("Cannot get delete file for default settings for network interfaces. This should not compulsorily lead into errors.\n\n" + e);
            }
        }

        /// <summary>
        /// Increases the time to check for connectivity for the current connection.
        /// </summary>
        public void IncreaseCheckTime()
        {
            networkConfigurations["current"].IncreaseCheckTime();
        }

        /// <summary>
        /// Tells whether the given name is equal to the current network manager.
        /// </summary>
        /// <param name="name">Name to check.</param>
        /// <returns>True if the name matches, false otherwise.</returns>
        public bool IsCurrentNetworkManager(string name)
        {
            if (networkConfigurations.ContainsKey(name))
                return networkConfigurations["current"].Equals(networkConfigurations[name]);

            return false;
        }

        /// <summary>
        /// Resets the connectivity check time to the default value of the current connection.
        /// </summary>
        public void ResetCheckTime()
        {
            networkConfigurations["current"].ResetCheckTime();
        }

        /// <summary>
        /// Get DNS servers belonging to the given network interface.
        /// </summary>
        /// <param name="nicId">ID of the network interface.</param>
        /// <returns>List of DNS server IPs.</returns>
        private static List<string> GetDnsServersById(string nicId)
        {
            List<string> ips = new List<string>();

            foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (networkInterface.Id == nicId)
                {
                    IPInterfaceProperties NicProperties = networkInterface.GetIPProperties();

                    foreach (IPAddress DnsAddress in NicProperties.DnsAddresses)
                        ips.Add(DnsAddress.ToString());
                }
            }

            return ips;
        }

        /// <summary>
        /// Retrieve the network interface with the given index.
        /// </summary>
        /// <param name="index">Index of the network interface to get.</param>
        /// <returns>The network interface belonging to the index.</returns>
        private static NetworkInterface GetNetworkInterfaceByIndex(uint index)
        {
            // Search in all network interfaces that support IPv4.
            NetworkInterface ipv4Interface = (from thisInterface in NetworkInterface.GetAllNetworkInterfaces()
                                              where thisInterface.Supports(NetworkInterfaceComponent.IPv4)
                                              let ipv4Properties = thisInterface.GetIPProperties().GetIPv4Properties()
                                              where ipv4Properties != null && ipv4Properties.Index == index
                                              select thisInterface).SingleOrDefault();
            if (ipv4Interface != null)
                return ipv4Interface;

            // Search in all network interfaces that support IPv6.
            NetworkInterface ipv6Interface = (from thisInterface in NetworkInterface.GetAllNetworkInterfaces()
                                              where thisInterface.Supports(NetworkInterfaceComponent.IPv6)
                                              let ipv6Properties = thisInterface.GetIPProperties().GetIPv6Properties()
                                              where ipv6Properties != null && ipv6Properties.Index == index
                                              select thisInterface).SingleOrDefault();

            return ipv6Interface;
        }

        /// <summary>
        /// Retrieve DNS settings. If the DnsFilePath exists restore settings, otherwise parse anew from network adapter.
        /// </summary>
        /// <returns>List with two entries: Settings-ID and DNS-string.</returns>
        private IDictionary<string, string> GetDefaultDNSSettings()
        {
            IDictionary<string, string> dnsSettings;

            if (File.Exists(dnsFilePath))
            {
                eventLog.LogWarning($"Found a {defaultDnsSettingsFile}. The service does not appear to be terminated correctly last time. Try to restore dns settings.");
                dnsSettings = RestoreDefaultDNSSettings(dnsFilePath);
                DeleteDNSSettingsFile();
            }
            else
            {
                dnsSettings = ParseDefaultDNSSettings();
            }

            // Save dns settings
            SaveDefaultDNSSettings(dnsSettings);

            return dnsSettings;
        }

        /// <summary>
        /// Get default DNS for given ID if set.
        /// </summary>
        /// <param name="interfaceId">ID to check.</param>
        /// <returns>DNS server or "auto" if ID not set.</returns>
        private string GetDnsServer(string interfaceId)
        {
            if (interfaceDnsSettingsMap.TryGetValue(interfaceId, out string dnsServer))
            {
                return dnsServer;
            }
            else
            {
                return "auto";
            }
        }

        /// <summary>
        /// Get all DNS server IPs from all ethernet nics or from one nic given by nicDesc.
        /// </summary>
        /// <param name="nicDescription">Optional: NIC description to change the dns server for.</param>
        /// <returns>List of all DNS server IPs.</returns>
        private List<string> GetDnsServerIps(string nicDescription = null)
        {
            // Returned list of IPs.
            List<string> ips = new List<string>();

            // Go through all network interfaces.
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Add IPs if the nicDescription matches or we don't have one.
                if (nicDescription == null || nic.Description == nicDescription)
                {
                    IPInterfaceProperties NicProperties = nic.GetIPProperties();
                    foreach (IPAddress DnsAddress in NicProperties.DnsAddresses)
                    {
                        ips.Add(DnsAddress.ToString());
                    }
                }
            }

            return ips;
        }

        /// <summary>
        /// Check if DNS is set to "Get DNS settings automatically"?
        /// </summary>
        /// <param name="NetworkAdapterGUID">GUID of the network adapter to check.</param>
        /// <returns>True if it is set to automatic, false otherwise.</returns>
        private bool IsDNSAuto(string NetworkAdapterGUID)
        {
            string path = interfaceConfigurationRegistryKey + NetworkAdapterGUID;
            string ns = (string) Registry.GetValue(path, "NameServer", null);

            return string.IsNullOrEmpty(ns);
        }

        /// <summary>
        /// Check whether the interface with the ID is in the list interfaceDnsSettingsMap.
        /// </summary>
        /// <param name="interfaceId">ID of the interface to check.</param>
        /// <returns>True if the interface is in the list, false otherwise.</returns>
        private bool IsInterfaceInList(string interfaceId)
        {
            return interfaceDnsSettingsMap.ContainsKey(interfaceId);
        }

        /// <summary>
        /// Shorthand to iterate over all network interfaces.
        /// </summary>
        /// <param name="callback">Function to call for each network interface.</param>
        private void IterateInterfaces(InterfaceIteratorCallback callback)
        {
            using (ManagementClass networkConfigMng = new ManagementClass("Win32_NetworkAdapterConfiguration"))
            using (ManagementObjectCollection networkConfigs = networkConfigMng.GetInstances())
            {
                foreach (ManagementObject managementObject in networkConfigs.Cast<ManagementObject>())
                {
                    // Only interfaces with IP
                    if ((bool) managementObject["IPEnabled"])
                    {
                        callback(managementObject);
                    }
                }
            }
        }

        /// <summary>
        /// Get the default DNS settings from the network adapter configuration.
        /// </summary>
        /// <returns>List with two entries: Settings-ID and DNS-string.</returns>
        private IDictionary<string, string> ParseDefaultDNSSettings()
        {
            var dnsSettings = new Dictionary<string, string>();

            try
            {
                IterateInterfaces((managementObject) =>
                {
                    string interfaceId = (string) managementObject["SettingID"];
                    string dnsServers = string.Join(",", GetDnsServersById(interfaceId));
                    string settingsId = (string) managementObject["SettingID"];

                    if (IsDNSAuto(settingsId))
                    {
                        dnsServers = "auto";
                    }

                    dnsSettings.Add(interfaceId, dnsServers);
                });
            }
            catch (Exception e)
            {
                eventLog.LogError(e, "Cannot get default settings for network interfaces.");
            }

            return dnsSettings;
        }

        /// <summary>
        /// Read default DNS settings from JSON-file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>List with two entries: Settings-ID and DNS-string.</returns>
        private Dictionary<string, string> RestoreDefaultDNSSettings(string filePath)
        {
            var defaultSettings = new Dictionary<string, string>();
            string jsonContent;

            try
            {
                jsonContent = File.ReadAllText(filePath);

                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonContent)))
                {
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(defaultSettings.GetType());
                    defaultSettings = deserializer.ReadObject(ms) as Dictionary<string, string>;
                }

                eventLog.LogInformation("Successfully restored dns settings from file:\n\n" + jsonContent);
            }
            catch (Exception e)
            {
                eventLog.LogError(e, "Cannot restore default DNS settings!"); // TODO: ITS.APE Client should STOP now!
            }

            return defaultSettings;
        }

        /// <summary>
        /// Save DNS settings to JSON-file.
        /// </summary>
        /// <param name="dnsSettings">List with two entries: Settings-ID and DNS-string.</param>
        private void SaveDefaultDNSSettings(IDictionary<string, string> dnsSettings)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(dnsSettings.GetType());

                    deserializer.WriteObject(memoryStream, dnsSettings);

                    using (var fileStream = File.Create(dnsFilePath))
                    {
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.CopyTo(fileStream);
                    }
                }
            }
            catch (Exception e)
            {
                eventLog.LogWarning(e, "Cannot store default DNS settings!");
            }
        }

        /// <summary>
        /// Set's the DNS Server of the local machine for entries in NicList.
        /// </summary>
        /// <param name="dnsServers">Comma separated list of DNS server addresses.</param>
        /// <remarks>Requires a reference to the System.Management namespace.</remarks>
        private void SetNameservers(string dnsServers)
        {
            SetNameservers(dnsServers, managementObject => networkInterfaces.Contains((string) managementObject["Caption"]));
        }

        /// <summary>
        /// Set's the DNS Server of the local machine for the given interface.
        /// </summary>
        /// <param name="dnsServers">Comma separated list of DNS server addresses.</param>
        /// <param name="interfaceId">Interface ID to set the nameserver for.</param>
        /// <remarks>Requires a reference to the System.Management namespace.</remarks>
        private void SetNameservers(string dnsServers, string interfaceId)
        {
            SetNameservers(dnsServers, managementObject => interfaceId == (string) managementObject["SettingID"]);
        }

        /// <summary>
        /// Set's the DNS Server of the local machine for the given interface check.
        /// </summary>
        /// <param name="dnsServers">Comma separated list of DNS server addresses.</param>
        /// <param name="interfaceCheck">Evaluation function to check whether the current managementObject fits the interface that should be configured.</param>
        /// <remarks>Requires a reference to the System.Management namespace.</remarks>
        private void SetNameservers(string dnsServers, Func<ManagementObject, bool> interfaceCheck)
        {
            IterateInterfaces((managementObject) =>
            {
                // Only look at interfaces succeeding the interfaceCheck.
                if (interfaceCheck(managementObject))
                {
                    List<string> oldDNS = GetDnsServerIps((string) managementObject["Description"]);

                    using (var newDNS = managementObject.GetMethodParameters("SetDNSServerSearchOrder"))
                    {
                        newDNS["DNSServerSearchOrder"] = (dnsServers == "auto") ? null : dnsServers.Split(',');

                        managementObject.InvokeMethod("SetDNSServerSearchOrder", newDNS, null);
                    }
                    // Add new DNS to interface list, too.
                    networkInterfaces.Add((string) managementObject["Caption"]);

                    // Check whether DNS is new.
                    List<string> setDNS = GetDnsServerIps((string)managementObject["Description"]);
                    eventLog.LogTrace($"Change DNS for interface {managementObject["Description"]}\nDNS HAS BEEN: {string.Join(",", oldDNS.ToArray())}\nSHOULD BE: {dnsServers}\n{(string) managementObject["Description"]}\nDNS is: {string.Join(",", setDNS.ToArray())}");
                }
            });
        }

        /// <summary>
        /// Encapsulate DLLImports.
        /// </summary>
        private static class NativeMethods
        {
            [DllImport("iphlpapi.dll", CharSet = CharSet.Auto)]
            public static extern int GetBestInterface(UInt32 destAddr, out UInt32 bestIfIndex);
        }
    }
}