﻿using ITSAPE_Client.Models;

namespace ITSAPE_Client.Utilities
{
    public interface INetworkManager
    {
        /// <summary>
        /// Interval for checking the connectivity.
        /// </summary>
        int ConnectivityInterval { get; set; }

        /// <summary>
        /// Add the NetworkConfiguration with name to the NetworkManager. If the name already exists or equals 'current' the NetworkConfiguration will not be added.
        /// </summary>
        /// <param name="name">The name of the NetworkConfiguration.</param>
        /// <param name="nc">The NetworkConfiguration to store.</param>
        /// <param name="isCurrentConfig">If true the NetworkConnection will be set as the 'current' connection.</param>
        /// <returns>True if the NetworkConnection has been added, false otherwise.</returns>
        bool AddConfiguration(string name, NetworkConfiguration nc, bool isCurrentConfig = false);

        /// <summary>
        /// Changes the current configuration.
        /// </summary>
        /// <param name="configName">The configuration to change to.</param>
        void Change(string configName);

        /// <summary>
        /// Check all network interfaces for their DNS config.
        /// Resets the config according to current network manager.
        /// </summary>
        void CheckInterfaces();

        /// <summary>
        /// Get rid of the DNS settings file.
        /// </summary>
        void DeleteDNSSettingsFile();

        /// <summary>
        /// Increases the time to check for connectivity for the current connection.
        /// </summary>
        void IncreaseCheckTime();

        /// <summary>
        /// Tells whether the given name is equal to the current network manager.
        /// </summary>
        /// <param name="name">Name to check.</param>
        /// <returns>True if the name matches, false otherwise.</returns>
        bool IsCurrentNetworkManager(string name);

        /// <summary>
        /// Resets the connectivity check time to the default value of the current connection.
        /// </summary>
        void ResetCheckTime();
    }
}