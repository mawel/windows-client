﻿using Newtonsoft.Json;

namespace ITSAPE_Client.Models
{
    /// <summary>
    /// Data class to hold a job.
    /// </summary>
    public class Job
    {
        [JsonProperty("deploy_path")]
        public string DeployPath { get; internal set; }

        [JsonProperty("file_path")]
        public string FilePath { get; internal set; }

        [JsonProperty("id")]
        public string Id { get; internal set; }

        [JsonProperty("status")]
        public string Status { get; internal set; }

        [JsonProperty("action")]
        public string Type { get; internal set; }

        [JsonProperty("user")]
        public string User { get; internal set; }

        [JsonProperty("watch_arguments")]
        public WatchTaskArguments WatchArguments { get; internal set; }

        public class WatchTaskArguments
        {
            [JsonProperty("reference_image_hash")]
            public string ReferenceImageHash { get; internal set; }

            [JsonProperty("artifact_name")]
            public string ArtifactName { get; internal set; }

            [JsonProperty("runtime_information")]
            public string RuntimeInformation { get; internal set; }

            [JsonProperty("detectors")]
            public string Detectors { get; internal set; }

            [JsonProperty("reference_image_path")]
            public string ReferenceImagePath { get; set; }
        }
    }
}