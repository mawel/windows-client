﻿namespace ITSAPE_Client.Models
{
    /// <summary>
    /// Data class to hold a response to the proxy.
    /// </summary>
    internal class ProxyResponse
    {
        /// <summary>
        /// Standard, empty constructor for setting values later.
        /// </summary>
        public ProxyResponse()
        {
        }

        /// <summary>
        /// Standard constructor setting all necessary parameters.
        /// </summary>
        /// <param name="message">Human readable message.</param>
        /// <param name="statusCode">Status code of response.</param>
        public ProxyResponse(string message, StatusCode statusCode)
        {
            Message = message;
            Status = statusCode;
        }

        internal enum StatusCode
        {
            Waiting,
            Completed,
            Failed
        }

        /// <summary>
        /// Human readable message.
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Status code of response.
        /// </summary>
        public StatusCode Status { get; private set; }
    }
}