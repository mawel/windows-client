﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("ITSAPE.Tests")]
namespace ITSAPE_Client.Models
{
    public class NetworkConfiguration
    {
        /// <summary>
        /// Current time interval to check online status (between DefaultCheckTime and MaxCheckTime.
        /// </summary>
        private int currentCheckTime;

        /// <summary>
        /// Default time interval to check online status.
        /// </summary>
        private int defaultCheckTime;

        /// <summary>
        /// Maximum time interval to check online status.
        /// </summary>
        private int maxCheckTime;

        /// <summary>
        /// Get new network configuration with the given parameters.
        /// </summary>
        /// <param name="dnsServers">DNS server to use.</param>
        /// <param name="gateway">DNS gateway server to use.</param>
        /// <param name="defaultCheckTime">Default time interval to check online status.</param>
        /// <param name="maxCheckTime">Maximum time interval to check online status.</param>
        public NetworkConfiguration(string dnsServers, string gateway, int defaultCheckTime = 5000, int maxCheckTime = 36000)
        {
            DnsServers = dnsServers;
            Gateway = gateway;

            this.defaultCheckTime = defaultCheckTime;
            currentCheckTime = defaultCheckTime;
            this.maxCheckTime = maxCheckTime;
        }

        /// <summary>
        /// >DNS server to use.
        /// </summary>
        public string DnsServers { get; private set; }

        /// <summary>
        /// DNS gateway server to use.
        /// </summary>
        public string Gateway { get; private set; }

        /// <summary>
        /// Override Equals to ensure two connections are equal if the DNS servers and the gateway are equal.
        /// </summary>
        /// <param name="obj">Object to compare.</param>
        /// <returns>True if the object is equals to this, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (obj is NetworkConfiguration other)
            {
                return other.DnsServers.Equals(DnsServers) && other.Gateway.Equals(Gateway);
            }

            return false;
        }

        /// <summary>
        /// Get hash code of this object.
        /// </summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Increases the connectivity check time by two; up to maxCheckTime.
        /// </summary>
        internal void IncreaseCheckTime()
        {
            currentCheckTime = currentCheckTime * 2;

            if (currentCheckTime >= maxCheckTime)
            {
                currentCheckTime = maxCheckTime;
            }
        }

        /// <summary>
        /// Resets the connectivity check time to the default value of this configuration.
        /// </summary>
        internal void ResetCheckTime()
        {
            currentCheckTime = defaultCheckTime;
        }

        /// <summary>
        /// Take over the configuration parameters from networkConfiguration.
        /// </summary>
        /// <param name="networkConfiguration"></param>
        internal void Set(NetworkConfiguration networkConfiguration)
        {
            DnsServers = networkConfiguration.DnsServers;
            Gateway = networkConfiguration.Gateway;

            defaultCheckTime = networkConfiguration.defaultCheckTime;
            currentCheckTime = networkConfiguration.currentCheckTime;
            maxCheckTime = networkConfiguration.maxCheckTime;
        }
    }
}