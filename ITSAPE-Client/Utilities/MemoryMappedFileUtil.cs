using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Security.AccessControl;

namespace ITSAPE.Client.Utilities
{
    public static class MemoryMappedFileUtil
    {
        public class MemoryMappedFileHandle : IDisposable
        {
            private IntPtr _mapping;

            private bool _disposing = false;
            public MemoryMappedFile MemoryMappedFile { get; }            
            
            internal MemoryMappedFileHandle(IntPtr mapping, MemoryMappedFile file)
            {
                _mapping = mapping;
                MemoryMappedFile = file;
            }

            ~MemoryMappedFileHandle()
            {
                Dispose();
            }


            public void Dispose()
            {
                if(_disposing)
                    return;
                _disposing = true;
                
                MemoryMappedFile?.Dispose();
                
                if(_mapping != IntPtr.Zero)
                    NativeMethods.CloseHandle(_mapping);
            }
        }
        
        public static MemoryMappedFileHandle CreateOrOpenFullAccess(string name, long size, MemoryMappedFileRights access)
        {
            if (TryOpenExisting(name, access, out var mf))
            {
                return new(IntPtr.Zero, mf); 
            }
            
            using var secAttribs = CreateSecAttribs();
            var mapping = NativeMethods.CreateFileMapping(
                UIntPtr.MaxValue,
                secAttribs,
                (uint)0x04,
                (uint)(size>>32),
                (uint)size,
                name);

            if (mapping == IntPtr.Zero)
            {
                uint lasterror = NativeMethods.GetLastError();
                throw new Win32Exception((int)lasterror, string.Format(CultureInfo.InvariantCulture, "Error creating shared memory. Errorcode is {0}", lasterror));
            }
            if (!TryOpenExisting(name, access, out mf))
            {
                throw new Exception("Cannot open created file mapping");
            }
            return new MemoryMappedFileHandle(mapping, mf);
        }

        public static bool TryOpenExisting(string name, MemoryMappedFileRights access, out MemoryMappedFile mapping)
        {
            mapping = null!;
            try
            {
                mapping = MemoryMappedFile.OpenExisting(name, access);
                return true;
            }
            catch (FileNotFoundException)
            {}
            return false;
        }

        private static NativeMethods.SECURITY_ATTRIBUTES CreateSecAttribs()
        {
            //Create the descriptor with a null DACL --> Everything is granted.
            RawSecurityDescriptor sec = new RawSecurityDescriptor(ControlFlags.DiscretionaryAclPresent, null, null, null, null);
            return new NativeMethods.SECURITY_ATTRIBUTES(sec);
        }
    }
}