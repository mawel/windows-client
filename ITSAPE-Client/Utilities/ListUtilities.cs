using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ITSAPE.Client.Utilities
{
    public static class ListUtilities
    {
        /// <summary>
        /// Boyer-Moore majority vote algorithm.
        /// </summary>
        /// <param name="dictionary">Get majority of this dictionaries entries.</param>
        public static int GetMajorityItem(this List<KeyValuePair<long, int>> dictionary)
        {
            int majorityItem = -1, counter = 0;

            foreach (var entry in dictionary)
            {
                if (counter == 0)
                {
                    majorityItem = entry.Value;
                    counter++;
                }
                else if (entry.Value == majorityItem)
                {
                    counter++;
                }
                else
                {
                    counter--;
                }
            }

            return majorityItem;
        }
    }
}