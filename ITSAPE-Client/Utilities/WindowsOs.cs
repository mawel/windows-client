﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ITSAPE.Client.Utilities
{
    public class WindowsOs: IOs
    {
        public bool CloseHandle(IntPtr handle)
        {
            return NativeMethods.CloseHandle(handle);
        }

        public bool CreateProcessAsUser(IntPtr hToken, string lpApplicationName, string lpCommandLine,
            NativeMethods.SECURITY_ATTRIBUTES lpProcessAttributes, NativeMethods.SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandle,
            int dwCreationFlags, IntPtr lpEnvrionment, string lpCurrentDirectory, ref NativeMethods.STARTUPINFO lpStartupInfo,
            ref NativeMethods.PROCESS_INFORMATION lpProcessInformation)
        {
            return NativeMethods.CreateProcessAsUser(hToken, lpApplicationName, lpCommandLine, lpProcessAttributes,
                lpThreadAttributes, bInheritHandle, dwCreationFlags, lpEnvrionment, lpCurrentDirectory,
                ref lpStartupInfo, ref lpProcessInformation);
        }

        public bool DuplicateTokenEx(IntPtr hExistingToken, int dwDesiredAccess, NativeMethods.SECURITY_ATTRIBUTES lpThreadAttributes,
            int ImpersonationLevel, int dwTokenType, ref IntPtr phNewToken)
        {
            return NativeMethods.DuplicateTokenEx(hExistingToken, dwDesiredAccess, lpThreadAttributes,
                ImpersonationLevel, dwTokenType, ref phNewToken);
        }

        public uint WTSGetActiveConsoleSessionId()
        {
            return NativeMethods.WTSGetActiveConsoleSessionId();
        }

        public bool WTSQueryUserToken(uint sessionId, out IntPtr Token)
        {
            return NativeMethods.WTSQueryUserToken(sessionId, out Token);
        }

        public int GetLastError()
        {
            return Marshal.GetLastWin32Error();
        }

        public int DnsFlushResolverCacheEntry(string hostName)
        {
            return NativeMethods.DnsFlushResolverCacheEntry(hostName);
        }
    }
}
