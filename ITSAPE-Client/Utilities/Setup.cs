﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ITSAPE_Client.Models;
using ITSAPE_Client.Utilities;
using Newtonsoft.Json;

namespace ITSAPE.Client.Utilities
{
    public class Setup : ISetup

    {
        /// <summary>
        /// File name of the JSON-encoded configuration.
        /// </summary>
        private const string _configurationFileName = "config.json";


        public Setup()
        {

        }

        public string ApplicationGuid => Assembly.GetExecutingAssembly().GetCustomAttribute<GuidAttribute>()?.Value ?? "ITS.APE Client";

        /// <summary>
        /// Load and parse the configuration file for the current program.
        /// </summary>
        /// <param name="configPath">Path to this program.</param>
        /// <returns>Config object from the JSON config file or null on error (client will stop!).</returns>
        public Configuration GetConfiguration()
        {
            using (StreamReader r = new StreamReader(Path.Combine(GetInstallPath(), _configurationFileName)))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<Configuration>(json);
            }
        }

        /// <summary>
        /// Get path where the program is installed, agnostic of the processor architecture.
        /// </summary>
        /// <returns>Absolute folder path.</returns>
        public string GetInstallPath()
            => GetContentRootPath();

        public static string GetContentRootPath()
        {
#if DEBUG
            return Environment.CurrentDirectory;
#else
            return Path.GetDirectoryName(Path.TrimEndingDirectorySeparator(AppContext.BaseDirectory));
#endif
        }
    }
}
