﻿namespace ITSAPE.Client.Utilities
{
    public interface ISystemHelper
    {
        /// <summary>
        /// Get information from the OS about the currently logged in user.
        /// </summary>
        /// <returns>Information string for the logged in user.</returns>
        string GetLoggedInUserInformation();
    }
}