using System;
using System.IO;
using ITSAPE.Client.Services;
using ITSAPE.Client.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.WindowsServices;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;

namespace ITSAPE.Client
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder UseApeWindowsService(this IHostBuilder hostBuilder, Action<WindowsServiceLifetimeOptions> configure)
        {
            if (WindowsServiceHelpers.IsWindowsService())
            {
                // Host.CreateDefaultBuilder uses CurrentDirectory for VS scenarios, but CurrentDirectory for services is c:\Windows\System32.
                hostBuilder.UseContentRoot(Setup.GetContentRootPath());
                hostBuilder.ConfigureLogging((hostingContext, logging) =>
                    {
                        logging.AddEventLog();
                    })
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddSingleton<ILifetimeEventDispatcher, LifetimeEventDispatcher>();
                        services.AddSingleton<IHostLifetime, ApeWindowsServiceLifetime>();
                        services.Configure<EventLogSettings>(settings =>
                        {
                            if (string.IsNullOrEmpty(settings.SourceName))
                            {
                                settings.SourceName = hostContext.HostingEnvironment.ApplicationName;
                            }
                        });
                        services.Configure(configure);
                    });
            }
            else throw new Exception("only as windows service");

            return hostBuilder;
        }
    }
}